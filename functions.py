from collections import OrderedDict
import json
import logging
from lockfile import LockFile

hdlr = logging.FileHandler('functions.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger = logging.getLogger('functions_logger')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


def update_file_data(new_data=None, **kwargs):
    try:
        new_data = new_data or OrderedDict()
        prev_data = get_file_data()
        new_data.update(kwargs)
        lock = LockFile("lockfile_file_data.lockfile")
        with lock:
            logger.info("Updating file data. new_data: %s prev_data: %s" % (new_data, prev_data))
            prev_data.update(new_data)
            prev_data['actions_to_perform'] = prev_data['actions_to_perform'].items()
            with open('tmp_data.json', 'w') as fil:
                fil.write(json.dumps(prev_data))
    except:
        logger.exception('exception in update_file_data')

def get_file_data(**kwargs):
    try:
        lock = LockFile("lockfile_file_data.lockfile")
        with lock:
            logger.info('Getting file data...')
            with open('tmp_data.json') as fil:
                data = json.loads(fil.read())
            data['actions_to_perform'] = OrderedDict(data['actions_to_perform'])
            return data
    except Exception as e:
        logger.warning('Failed obtaining data from file. Exception: %s' % repr(e))
        logger.info("Creating new data file...")
        try:
            with open('tmp_data.json', 'w') as fil:
                initial_data = {
                    'actions_to_perform': [],
                    'stop': 'False'
                }
                fil.write(json.dumps(initial_data))
            initial_data['actions_to_perform'] = OrderedDict()
            return initial_data
        except:
            logger.exception('Data file creation failed.')
            raise 


