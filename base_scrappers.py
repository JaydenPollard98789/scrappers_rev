# -*- coding: utf-8 -*-
import os
from sys import exit
import platform
import inspect
import json
import re
from splinter import Browser
from time import sleep
import datetime
import logging
import random
import requests
import traceback
from threading import Thread

from selenium.common.exceptions import StaleElementReferenceException, NoSuchWindowException
import httplib2
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select


from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options

from selenium.webdriver.support.ui import WebDriverWait
from splinter.driver.webdriver import BaseWebDriver, WebDriverElement
from selenium import webdriver
from selenium.common.exceptions import TimeoutException

import settings
from functions import get_file_data

class WaitCaseTimeout(Exception):
    pass

class WaitUntilTimeout(Exception):
    pass

class WaitOrScrollTimeout(Exception):
    pass

class TryOrWaitTimeoutException(Exception):
    pass

class VisitMaxRetriesException(Exception):
    pass

class StopScrapperEventFired(Exception):
    pass


class BaseScrapper(object):
    def __init__(self, *args, **kwargs):
        hdlr = logging.FileHandler('scrapper_performer.log')
        hdlr = logging.FileHandler('scrapper_performer.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger = logging.getLogger('scrapper_performer')
        self.logger.addHandler(hdlr)
        self.page_load_timeout = 18
        self.timeout_try_or_wait = 180
        self.logger.setLevel(logging.INFO)
        self.active_stop_event = False
        self.stop = False
        cap = DesiredCapabilities().FIREFOX
        cap["marionette"] = True

        try:
            pass
            self.logged = False
        except:
            raise
            exit()

    def stop_condition(self):
        data = get_file_data()
        return data['stop'] == 'True'

    def _get_cookies(self):
        try:
            cookies = pickle.load(open("cookies.pkl", "rb"))
            for cookie in cookies:
                self.browser.driver.add_cookie(cookie)
        except IOError:
            pass
        except EOFError:
            print ("Se guardaron mal las cookies!")

    def jsclick(self, webdriver_element):
        self.try_or_wait(lambda: self.browser.driver.execute_script("$(arguments[0]).click();", webdriver_element), raise_classes=[StaleElementReferenceException])

    def try_or_wait(self, func, *args, **kwargs):
        max_seconds = kwargs.pop("max_sec", None)
        max_sec = max_seconds or self.timeout_try_or_wait or 10
        raise_classes = kwargs.pop('raise_classes', [])
        show_url = kwargs.pop('show_url', False)
        finish_time = datetime.datetime.now() + datetime.timedelta(seconds=max_sec)
        url = ''
        if show_url:
            for i in range(15):
                try:
                    url = self.browser.url
                    break
                except:
                    url = "No se pudo obtener url"
                    self.sleep(0.8)
        while datetime.datetime.now() <= finish_time:
            try:
                if self.active_stop_event and self.stop_condition():
                    raise StopScrapperEventFired
                print (inspect.getsource(func))                    
                return func(*args, **kwargs)
            except StopScrapperEventFired:
                raise
            except Exception as e:
                if e.__class__ in raise_classes:
                    raise e
                exc = traceback.format_exc()
                self.logger.warning("Exception handled in try_or_wait method.\n\nFunction: {function}\n\nTraceback:\n{traceback}".format(traceback=exc, function=inspect.getsource(func)))
                self.sleep(0.2)
        raise TryOrWaitTimeoutException('Tiempo de espera terminado. Por favor verifique su conexión a internet.')

    def _wait_until(self, func, *args, **kwargs):
            raise_classes = kwargs.pop('raise_classes', [])
            max_sec = kwargs.pop("max_sec", False)
            no_raise_timeout = kwargs.pop("no_raise_timeout", False)
            finish_time = (datetime.datetime.now() + datetime.timedelta(seconds=max_sec)) if max_sec else False
            condition = lambda: True
            # import pdb; pdb.set_trace()

            if finish_time:
                condition = lambda: datetime.datetime.now() < finish_time
            self.logger.info("esperando... function: \n{}".format(repr(inspect.getsource(func))))
            while condition():
                try:
                    if self.active_stop_event and self.stop_condition():
                        raise StopScrapperEventFired
                    res = func(*args, **kwargs)
                    if res:
                        self.logger.info('espera finalizada.')
                        return res
                except StopScrapperEventFired:
                    raise
                except Exception as e:
                    if e.__class__ in raise_classes:
                        raise e                    
                    exc = traceback.format_exc()
                    # self.logger.warning("Exception handled in _wait_until.\n\n{url}\n\nFunction: {function}\n\nTraceback:\n{traceback}".format(url="Url:" + url if show_url else '', traceback=exc, function=inspect.getsource(func)))
                self.sleep(0.4)
            if not no_raise_timeout:
                raise WaitUntilTimeout

    def try_or_scroll(self, func, *args, **kwargs):
        raise_classes = kwargs.pop('raise_classes', [])
        webdriver_element = kwargs.pop('webdriver_element', [])
        scroll_from_current_position = kwargs.pop('scroll_from_current_position', False)
        reverse_from_current_position = kwargs.pop('reverse_from_current_position', False)
        doc_height = self.try_or_wait(lambda: self.browser.driver.execute_script("""var body = document.body, html = document.documentElement; return Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight );"""))
        h = self.try_or_wait(lambda: self.browser.driver.execute_script('return window.pageYOffset;')) if scroll_from_current_position or reverse_from_current_position else 0
        assert func
        if reverse_from_current_position:
            condition = lambda h: h >= 0
        else:
            condition = lambda h: h <= doc_height
        while condition(h):
            self.dev_logger.info("scrolling page...")
            try:
                if self.is_element_completely_visible(webdriver_element):
                    return func(*args, **kwargs)
                else:
                    if reverse_from_current_position:
                        h -= 5
                    else:
                        h += 5
                    self.try_or_wait(lambda: self.browser.execute_script("window.scrollTo(0, %d)" % h), show_url=False)                    
                    self.sleep(0.3)
            except httplib2.CannotSendRequest:
                pass
            except Exception as e:
                if e.__class__ in raise_classes:
                    raise e
                self.dev_logger.exception("exception in try_or_scroll")
                if not self.is_element_completely_visible(webdriver_element):
                    if reverse_from_current_position:
                        h -= 5
                    else:
                        h += 5
                    self.try_or_wait(lambda: self.browser.execute_script("window.scrollTo(0, %d)" % h), show_url=False)
                    self.sleep(0.3)
        self.dev_logger.info("try_or_scroll in bottom of page. Function end.")
        raise Exception("BottomOfPageRaised")


    def is_elemetry_or_scroll(self, webdriver_element):
        return self.browser.driver.execute_script("""
            function elementInViewport(el) {
              var top = el.offsetTop;
              var left = el.offsetLeft;
              var width = el.offsetWidth;
              var height = el.offsetHeight;

              while(el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
              }

              return (
                top < (window.pageYOffset + window.innerHeight) &&
                left < (window.pageXOffset + window.innerWidth) &&
                (top + height) > window.pageYOffset &&
                (left + width) > window.pageXOffset
              );
            }
            return elementInViewport(arguments[0])""", webdriver_element)

    def is_element_completely_visible(self, webdriver_element):
        return self.browser.driver.execute_script("""
            function elementInViewport(el) {
              var top = el.offsetTop;
              var left = el.offsetLeft;
              var width = el.offsetWidth;
              var height = el.offsetHeight;

              while(el.offsetParent) {
                el = el.offsetParent;
                top += el.offsetTop;
                left += el.offsetLeft;
              }

              return (
                top >= window.pageYOffset &&
                left >= window.pageXOffset &&
                (top + height) <= (window.pageYOffset + window.innerHeight) &&
                (left + width) <= (window.pageXOffset + window.innerWidth)
              );
            }
            return elementInViewport(arguments[0])""", webdriver_element)        

        
    def scroll_to_element(self, webdriver_element, *args, **kwargs):
        y_change = kwargs.pop('y_change', 0)
        y_position = self.try_or_wait(lambda: self.browser.driver.execute_script("return arguments[0].getBoundingClientRect().top + window.scrollY", webdriver_element), raise_classes=[StaleElementReferenceException])
        self.try_or_wait(lambda: self.browser.execute_script("window.scrollTo(0, %d)" % (y_position + y_change)), raise_classes=[StaleElementReferenceException])


    def wait_cases(self, *cases_list, **kwargs):
        infinite_retry = kwargs.pop('infinite_retry', False)
        max_sec = kwargs.pop("max_sec", 120)
        finish_time = datetime.datetime.now() + datetime.timedelta(seconds=max_sec)
        while infinite_retry or (finish_time > datetime.datetime.now()):
            try:
                for case_data in cases_list:
                    res = self.try_or_wait(case_data['function'], *case_data.get('args', []), max_sec=6, **case_data.get('kwargs', {}))
                    if res:
                        return case_data['name'], res
            except:
                self.logger.exception('exception in wait_cases')
            self.sleep(0.4)
        raise WaitCaseTimeout

    def find_by_exact_text(self,  text):
        return self.browser.driver.find_element_by_xpath("//*[text()='%s']" % text)

    def find_by_partial_text(self, text):
        return self.browser.driver.find_by_xpath("//div[contains(text(), '%s')]" % text)

    def change_window(self, position='last', window=None):
        if window:
            return self.try_or_wait(lambda: 
                self.browser.driver.switch_to_window(window),
            raise_classes=[NoSuchWindowException])
        else:
            if position == 'last':
                index = -1
            elif position == 'first':
                index = 0
            else:
                index = position
            return self.try_or_wait(lambda: 
                self.browser.driver.switch_to_window(self.get_windows()[index]),
            raise_classes=[NoSuchWindowException])

    def get_windows(self):
        # return self.try_or_wait(lambda: self.browser.driver.window_handles) 
        return self.browser.driver.window_handles

    def ask(self, question, options=('y', 'n'), default=None):
        answer = None
        while not answer:
            answer = raw_input(question)
            answer = answer.strip().lower()
            if not answer:
                if default is not None:
                    return default
                else:
                    print ("you have to answer. Options: %s" % ', '.join(options))
            elif answer in options:
                return answer
            else:
                answer = None
                print ("that is not a valid option. Options: %s" % ', '.join(options))

    def open_window(self):
        try:
            self.try_or_wait(lambda: self.browser.driver.execute_script('window.open()'), max_sec=10, raise_classes=[NoSuchWindowException])
        except NoSuchWindowException:
            self.logger.warning('NoSuchWindowException handled in open_window')
            self.change_window()
            self.try_or_wait(lambda: self.browser.driver.execute_script('window.open()'), max_sec=10, raise_classes=[NoSuchWindowException])
        except TryOrWaitTimeoutException:
            self.change_window()
        self.sleep(0.6)

    def close_window(self, position='current'):
        if position == 'current':
            self.try_or_wait(lambda: self.browser.windows.current.close())
        else:
            self.try_or_wait(lambda: self.browser.windows[position].close())
        self.sleep(0.6)

    # def visit(self, url, timeout=60, retry=10, interactive=True):
    #     return self.try_or_wait(lambda: self.browser.driver.get(url))
    
    def visit(self, url, timeout=60, retry=10, interactive=True):
        
        return self.try_or_wait(lambda: self.browser.get('https://{}'.format(url)))

    def timer(minutes):
        self.sleep(minutes * 60)
        self.alert()

    def sleep(self, time):
        init_function_time = datetime.datetime.now()
        function_time_spent = 0
        transc = time
        print ("Esperando %s segundos..." % time)
        while transc > 0:
            if self.active_stop_event and self.stop_condition():
                raise StopScrapperEventFired
            if init_function_time:
                function_time_spent = datetime.datetime.now() - init_function_time
                function_time_spent = function_time_spent.total_seconds()
                # sleep_time = 5 if 5 > transc + function_time_spent else transc
                if transc - function_time_spent > 0:
                    transc -= function_time_spent
                    function_time_spent = 0
                    sleep_time = 5 if 5 >= transc else transc
                else:
                    sleep_time = 0
                    transc = 0
            else:
                sleep_time = 5 if 5 >= transc else transc
            sleep(sleep_time)
            transc -= sleep_time
        return True


class GlobalBaseScrapper(BaseScrapper):
    default_user_agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
    # default_user_agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
    global_profile_url = 'https://www.globaltestmarket.com/myprofile.php'

    def __init__(self,url='rev.ai'):
        hdlr = logging.FileHandler('scrapper_performer.log')
        hdlr = logging.FileHandler('scrapper_performer.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger = logging.getLogger('scrapper_performer')
        self.logger.addHandler(hdlr)
        self.page_load_timeout = 18
        self.timeout_try_or_wait = 180
        self.logger.setLevel(logging.INFO)
        self.active_stop_event = False
        self.stop = False

        cap = DesiredCapabilities().FIREFOX
        cap["marionette"] = True
        browser = webdriver.Firefox(firefox_binary=settings.FIREFOX_BINARY_DIR,capabilities=cap,executable_path=settings.DRIVER_FIREFOX)
        self.try_or_wait(lambda:browser.get('https://{}'.format(url)))
        import pdb; pdb.set_trace()
        self.try_or_wait(lambda:browser.find_by_id('account-signup-menu').click())
                


        
        

    def create_account(self):    
        self.change_window(position='first')
        self.visit("https://www.aol.com/login/?lang=en-us&intl=us&src=fp-us")
        self.try_or_wait(lambda: self.browser.fill('firstName', context['first_name']))
        self.try_or_wait(lambda: self.browser.fill('lastName', context['last_name']))
        self.try_or_wait(lambda: self.browser.fill('yid', context['email']))
        self.try_or_wait(lambda: self.browser.find_by_css('#desktop-suggestion-list > li:nth-child(4)').click())
        context['email'] = self.try_or_wait(lambda: self.browser.find_by_name('yid').value)
        self.try_or_wait(lambda: self.browser.fill('password', context['password']))
        self.try_or_wait(lambda: self.browser.select('shortCountryCode', context['short_country_code']))
        self.try_or_wait(lambda: self.browser.fill('phone', context['phone']))
        month, day, year = map(lambda num: str(int(num)), context['birth_date'].split('-'))
        self.try_or_wait(lambda: Select(self.browser.driver.find_element_by_name('mm')).select_by_value(str(month)))
        self.try_or_wait(lambda: self.browser.fill('dd', day))
        self.try_or_wait(lambda: self.browser.fill('yyyy', year))
        self.try_or_wait(lambda: self.browser.find_by_name('freeformGender').click())
       
        self.try_or_wait(lambda: self.browser.find_by_css('#reg-gender-list > li:nth-child(2)').click())
        chosen_email = self.try_or_wait(lambda: self.browser.find_by_name('yid').value) + '@aol.com'
        requests.get("%s/person_creation_data/" % self.server_url(), params=dict(email=chosen_email, id=context['person_id']))
        self.try_or_wait(lambda: self.browser.find_by_id('reg-submit-button').click())
        self.try_or_wait(lambda: self.browser.find_by_name('sendCode').click())

        return 'success'

    
    def fill_global_account_data(self, context={}, street='', city='', postal_code=''):
        self.change_window()
        # self.visit('https://www.globaltestmarket.com/')
        self.visit('https://www.globaltestmarket.com/join.php?utm_source=panthera&utm_medium=affiliate&utm_campaign=pantherauk7&p=pantherauk7&lang=E&CONTACT_COUNTRY=UK&redirect=false&CID=3004')
        # @TODO: check reggender field in browser inspector
        self.try_or_wait(lambda: self.browser.find_by_id('reggender1').click())
        # self.try_or_wait(lambda: self.browser.find_by_name('regFirstName').click())
        # self.try_or_wait(lambda: self.browser.fill('regFirstName', context['first_name']))
        self.sleep(1)
        self.try_or_wait(lambda: self.browser.find_by_id('regFirstName').fill(context['first_name']))
        self.sleep(3)
        self.try_or_wait(lambda: self.browser.find_by_id('regLastName').fill(context['last_name']))
        self.sleep(3)
        self.try_or_wait(lambda: self.browser.find_by_id('regEmail').fill(context['email']))
        self.sleep(1)
        self.try_or_wait(lambda: self.browser.find_by_id('newRegFormBtn').click())
        self.sleep(2)
        self.try_or_wait(lambda: Select(self.browser.driver.find_element_by_name('contact_country')).select_by_value('UK'))
        month, day, year = map(lambda num: str(int(num)), context['birth_date'].split('-'))
        self.sleep(3.5)
        self.try_or_wait(lambda: Select(self.browser.driver.find_element_by_name('birthmonth')).select_by_value(month))
        self.sleep(2)
        self.try_or_wait(lambda: Select(self.browser.driver.find_element_by_name('birthday')).select_by_value(day))
        self.sleep(3)
        self.try_or_wait(lambda: Select(self.browser.driver.find_element_by_name('birthyear')).select_by_value(year))
        self.sleep(5)
        self.try_or_wait(lambda: self.browser.fill('contact_street1', street))
        self.sleep(3)
        self.try_or_wait(lambda: self.browser.fill('contact_city', city))
        self.sleep(4)
        self.try_or_wait(lambda: self.browser.fill('contact_zipcode', postal_code))
        # self.try_or_wait(lambda: self.browser.fill('contact_state', context['state']))
        self.sleep(6)
        self.try_or_wait(lambda: self.browser.fill('userpass', context['global_password']))
        self.sleep(2)
        self.try_or_wait(lambda: self.browser.fill('cuserpass', context['global_password']))
        self.sleep(1)
        self.try_or_wait(lambda: self.browser.find_by_name('user_agreement_date').click())        

    def create_global_account(self, context={}):
        make_process = True
        scr = self.get_ip_watching_scrapper()
        while make_process:
            try:
                raw_input('Hit enter when the browser settings are ready to start...')
                if context['status'] == 'unus':
                    self.set_uk_tz()
                    init_ip = scr.check_whoer()['ip']

                    scr.visit('https://myip.es/')

                    if self.ask('Is this city ok or it must be changed?[o/c]: ', options=('o', 'c')) == 'c':
                        continue
                    street_number = str(random.randint(1, 14))
                    street_number = '0' + street_number if len(street_number) == 1 else street_number

                    street = raw_input('Paste street (without street number): ').strip()
                    postal_code = raw_input('Pase zipcode: ').strip()
                    city = raw_input('Pase city: ').strip()
                    street = '%s %s' % (street_number, street)
                    scr.change_window()

                    current_ip = scr.check_whoer()['ip']
                    if current_ip != init_ip:
                        print ("The IP has changed, restarting process...")
                        continue
                    work_scr = self.get_work_scrapper()
                    
                    print ("street: %s" % street)
                    print ("postal_code: %s" % postal_code)
                    requests.get("%s/global_account_creation_data/" % work_scr.server_url(), 
                        params=dict(
                            id=context['person_id'],
                            street=street,
                            postal_code=postal_code,
                            city=city,
                        )
                    )
                    work_scr.open_window()
                    work_scr.change_window(position='first')
                    work_scr.close_window()

                    work_scr.change_window()
                    work_scr.fill_global_account_data(context=context, street=street, postal_code=postal_code, city=city)
                    while True:
                        if work_scr.ask('The account was created? [y/n]') == 'y':
                            work_scr.open_window()
                            work_scr.change_window()
                            work_scr.visit("https://www.aol.com/login/?lang=en-us&intl=us&src=fp-us")
                            work_scr.try_or_wait(lambda: work_scr.browser.fill('username', context["email"]))
                            work_scr.try_or_wait(lambda: work_scr.browser.find_by_id("login-signin").click())
                            work_scr.try_or_wait(lambda: work_scr.browser.fill('password', context["password"]))
                            work_scr.try_or_wait(lambda: work_scr.browser.find_by_id("login-signin").click())

                            account_status = None
                            if work_scr.ask('Gatito guta o no guta pecaito? [y/n]') == 'y':
                                account_status = 'acti'
                            else:
                                account_status = 'nega'
                            print ("Remember change your location in Tuxler if you are going to create another global account now...")
                            return 'success'
                        else:
                            if work_scr.ask('retry? (just global account creation part)[y/n]: ') == 'y':
                                work_scr.fill_global_account_data(context=context, street=street, postal_code=postal_code, city=city)
                            elif work_scr.ask('restart process with other Tuxler location?[y/n]: ') == 'y':
                                break
                            else:
                                return 'error'
                else:
                    print ("The Person should have status 'unused' to be valid for global account creation")
            except StopScrapperEventFired:
                raise
            except:
                work_scr.logger.exception('exception in create_global_account method')
                traceback.print_exc()
                retry_ans = work_scr.ask('retry, not retry, or reload scrapper code and retry? [r/n/re]: ', options=['r', 'n', 're'])
                if retry_ans == 're':
                    print ("# @TODO: Programa esta accion ;)")
                    return 'error'
                elif retry_ans == 'n':
                    return 'error'


    def server_url(self):
        return "http://%s:%s" % (settings.HOST, settings.HOST_PORT)

    # def keep_session_active(self):
    #     self.get_global_window()
    #     while True:
    #         try:
    #             # self.try_or_wait(lambda: self.browser.refresh())
    #             if self.is_ip_valid:
    #                 self.browser.driver.refresh()
    #         except:
    #             traceback.print_exc()
    #             print ("refrescando pagina...")
    #             pass
    #         # button = self.try_or_wait(lambda: self.browser.find_by_css("p.botonera:nth-child(3) > a:nth-child(1)"))
    #         # if button:
    #         #     try:
    #         #         self.try_or_wait(lambda: button.click(), raise_classes=[StaleElementReferenceException])
    #         #     except StaleElementReferenceException:
    #         #         pass
    #         #
    #         self.sleep(6 * 60)


    # def solve_polls(self):
    #     #click on search polls button to start and see if there is some poll available
    #     pass

    # def get_available_surveys(self):
    #     surveys_data = {}
    #     self.get_global_window()
    #     survey_buttons = self.browser.find_by_css("#article > div > table:nth-child(2) > tbody > tr > td:nth-child(2) > a")
    #     for survey in survey_buttons:
    #         link = survey['href']
    #         link_split = link.split('E_')[1].split('_')
    #         survey_number = link_split[0]
    #         survey_url_code = link.split('&')[0] 
    #         surveys_data[survey_number] = dict(element=survey, survey_url_code=survey_url_code)
    #     return surveys_data

    # def is_in_global(self):
    #     try:
    #         return 'www.globaltestmarket.com' in self.try_or_wait(lambda: self.browser.url, raise_classes=[NoSuchWindowException])
    #     except NoSuchWindowException:
    #         self.change_window()
    #         return 'www.globaltestmarket.com' in self.try_or_wait(lambda: self.browser.url, raise_classes=[NoSuchWindowException])


    # def get_global_window(self, profile=False):
    #     global_profile_url = 'https://www.globaltestmarket.com/myprofile.php'
    #     for window in self.get_windows():
    #         try:
    #             self.change_window(window=window)
    #             # if self.try_or_wait(lambda: window.url, raise_classes=[NoSuchWindowException]) == 'https://www.globaltestmarket.com/myprofile.php':
    #             # if self.try_or_wait(lambda: self.browser.url) == 'https://www.globaltestmarket.com/myprofile.php':
    #             if profile and self.try_or_wait(lambda: self.browser.url == global_profile_url):
    #                 return window
    #             elif self.is_in_global():
    #                 return window
    #         except NoSuchWindowException:
    #             pass

    # def keep_global_session(self):
    #     def _session_loop():
    #         window = self.get_global_window()
    #         global_profile_url = 'https://www.globaltestmarket.com/myprofile.php'
    #         if not window:
    #             self.exception('there is no global window with url: ' + global_profile_url)
    #             return False
    #         self.logger.info("Starting keep session function...")
    #         while self.is_session_active:
    #             # self.change_window(window=window)
    #             # if self.active_stop_event and self.stop_condition():
    #             #     raise StopScrapperEventFired
    #             # except StopScrapperEventFired:
    #             #     raise
    #             if not self.is_in_global():
    #                 window = self.get_global_window()
    #             if window and self.try_or_wait(lambda: self.browser.url) == global_profile_url:
    #                 whoer_data = self.get_ip_watching_scrapper().check_whoer()
    #                 if whoer_data['is_anon'] or self.force_whoer:
    #                     self.logger.info('Refreshing to keep global session... whoer_data: %s' % repr(whoer_data))
    #                     print ('Refreshing to keep global session... whoer_data: %s' % repr(whoer_data))
    #                     # self.visit(global_profile_url)
    #                     self.try_or_wait(lambda: self.browser.driver.refresh())
    #                 # @TODO: validar cambio de ubicacion, valida, invalida, en modo semi automatico y modo totalmente automatico (silencioso)
    #             time_to_refresh = random.randint(6 * 60, 10 * 60)
    #             self.logger.info('Waiting %s minutes to refresh to keep global session' % (time_to_refresh / 60.0))
    #             self.sleep(time_to_refresh)
    #     self.session_thread = Thread(target=_session_loop)
    #     self.session_thread.start()
    #     return self.session_thread

    # def solve_direct_poll_with_hack(self, poll_number=None, minutes=None, seconds=0):
    #     # survey_buttons = self.browser.find_by_css('tr.GTMonlineSurvey .btnBig.blue1')
    #     surveys_data = self.get_available_surveys()
    #     poll_number = str(poll_number)
    #     if poll_number in surveys_data:
    #         surveys_data[poll_number]['element'].click()
    #         hack_link = 'http://globaltestmarket.com/20/survey/finished.phtml?ac=E_{ide}_{url_code}&sn={ide}&lang=E'.format(
    #             ide=poll_number,
    #             url_code=surveys_data[poll_number]['survey_url_code']
    #         )
    #         self.logger.info('Waiting %s minutes and %s seconds to submit hack: %s' % (minutes * 60, seconds, hack_link))
    #         self.sleep((minutes * 60) + seconds)
    #         self.logger.info('Submitting hack_link: %s' % hack_link)
    #         self.change_window()
    #         # self.visit(hack_link)
    #         # @TODO: hacer que verifique que el hack haya sido aceptado y mande al server status y datos de la accion y los puntos ganados
            
    #         return True
    #     else:
    #         self.logger.warning("The survey %s doesn't exist")
    #         return False


   

    # def clear_cache(self, timeout=60):
    #     self.try_or_wait(lambda: self.browser.driver.execute_script('window.open()'))
    #     self.try_or_wait(
    #         lambda: self.browser.driver.switch_to_window(self.browser.driver.window_handles[-1])
    #     )
    #     self.visit('chrome://settings/clearBrowserData')
    #     self.jsclick(self.browser.find_by_css("#clearBrowsingDataConfirm"))

    def stop_scrapper_action(self):
        self.stop = True
        return True

   
   

    def create_life_point(self):
        scr=self
        scr.clear_history()
        scr.clear_cookies()
        raw_input("Create Accounts LifePoints...")
        scr.change_window(position='first')
        scr.visit("https://www.lifepointspanel.com/registration")

        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-first-name--2').fill('jesus'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-last-name--2').fill('villalta'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-email-address--4').fill('villala@gmail.com'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-confirm-email-address--2').fill('villala@gmail.com'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-password--4').fill('123456@Js'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-password-confirm--3').fill('123456@Js'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-nextbutton--2').click())

        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-gender--2 > option:nth-child(2)').click())
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-mailing-address1--2').fill('27-11-1995'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-state--2 > option:nth-child(2)').click())
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-city--2').fill('27-11-1995'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-postal-code--2').fill('27-11-1995'))
        scr.try_or_wait(lambda: scr.browser.find_by_css('#edit-nextbutton1--2').click())
            
        return