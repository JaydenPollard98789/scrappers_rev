import json
import logging

from flask import Flask, request

from functions import update_file_data, get_file_data

app = Flask(__name__)
hdlr = logging.FileHandler('api.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger = logging.getLogger('api_logger')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


@app.route('/set_action', methods=['POST'])
def set_action():
    post_data = request.json
    print "received: ", repr(post_data)
    action_data = post_data
    if action_data['action'] == 'stop_scrapper_action':
        update_file_data(stop='True')    
    else:
        data = get_file_data()
        actions_to_perform = data['actions_to_perform']
        actions_to_perform[action_data['action_id']] = action_data
        update_file_data(actions_to_perform=actions_to_perform)
    data = {'status': True}
    return json.dumps(data) 



if __name__ == '__main__':
    app.run(port=8084)    