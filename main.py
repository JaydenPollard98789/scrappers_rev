# -*- coding: utf-8 -*-
import os
from sys import exit
import platform
import inspect
import json
import re
import datetime
import logging
import requests
import traceback
import random
from threading import Thread
from time import sleep
from lockfile import LockFile

# splinter
from splinter import Browser
# selenium
from selenium.common.exceptions import StaleElementReferenceException, NoSuchWindowException
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import Select
from selenium.webdriver import Firefox
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
# form selenium.webdriver.common.keys import Keys
from splinter.driver.webdriver import BaseWebDriver, WebDriverElement
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
# telegram
import telegram
from telegram.error import NetworkError, Unauthorized

from functions import get_file_data, update_file_data
# import diccionario
import settings
import data_person

class WaitCaseTimeout(Exception):
    pass

class WaitUntilTimeout(Exception):
    pass

class WaitOrScrollTimeout(Exception):
    pass

class TryOrWaitTimeoutException(Exception):
    pass

class VisitMaxRetriesException(Exception):
    pass

class StopScrapperEventFired(Exception):
    pass



class main(object):
    """docstring for main"""
    def __init__(self):
        hdlr = logging.FileHandler('scrapper_performer.log')
        hdlr = logging.FileHandler('scrapper_performer.log')
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger = logging.getLogger('scrapper_performer')
        self.logger.addHandler(hdlr)
        self.page_load_timeout = 18
        self.timeout_try_or_wait = 180
        self.logger.setLevel(logging.INFO)
        self.active_stop_event = False
        self.stop = False
        
        self.browser= Browser('firefox')
        input('------> Debe de Abrir Dos tabs <------ \n Enter para continuar')
        numeros_cuentas_create= int(input('Cuantos Tokens Necesitas: '))
        for x in range(0,numeros_cuentas_create):
            # import pdb;pdb.set_trace()
            print('{}-Token'.format(x))
            data=PersonRandom.person()
            # crear correo ramdom
            # import pdb;pdb.set_trace()
            get_email= self.get_email()
            # crear rev primer paso
            self.RevCreate(email=get_email,context=data)
            # confirmacion
            url_confirm=self.confirm_rev_email()
            # login
            # confirmar cuenta
            self.loginRev(url_confirm,get_email,data['password'])
            # borrar cookies
            self.browser.cookies.delete()
            
        import pdb; pdb.set_trace()
        print ("")
        print ("")
        print ("")
        print ("")

       
    def try_or_wait(self, func, *args, **kwargs):
        max_seconds = kwargs.pop("max_sec", None)
        max_sec = max_seconds or self.timeout_try_or_wait or 10
        raise_classes = kwargs.pop('raise_classes', [])
        show_url = kwargs.pop('show_url', False)
        finish_time = datetime.datetime.now() + datetime.timedelta(seconds=max_sec)
        url = ''
        if show_url:
            for i in range(15):
                try:
                    url = self.browser.url
                    break
                except:
                    url = "No se pudo obtener url"
                    self.sleep(0.8)
        while datetime.datetime.now() <= finish_time:
            try:
                if self.active_stop_event and self.stop_condition():
                    raise StopScrapperEventFired
                print (inspect.getsource(func))                    
                return func(*args, **kwargs)
            except StopScrapperEventFired:
                raise
            except Exception as e:
                if e.__class__ in raise_classes:
                    raise e
                exc = traceback.format_exc()
                self.logger.warning("Exception handled in try_or_wait method.\n\nFunction: {function}\n\nTraceback:\n{traceback}".format(traceback=exc, function=inspect.getsource(func)))
                self.sleep(0.2)
        raise TryOrWaitTimeoutException('Tiempo de espera terminado. Por favor verifique su conexión a internet.')

    def sleep(self, time):
        init_function_time = datetime.datetime.now()
        function_time_spent = 0
        transc = time
        print ("Esperando %s segundos..." % time)
        while transc > 0:
            if self.active_stop_event and self.stop_condition():
                raise StopScrapperEventFired
            if init_function_time:
                function_time_spent = datetime.datetime.now() - init_function_time
                function_time_spent = function_time_spent.total_seconds()
                # sleep_time = 5 if 5 > transc + function_time_spent else transc
                if transc - function_time_spent > 0:
                    transc -= function_time_spent
                    function_time_spent = 0
                    sleep_time = 5 if 5 >= transc else transc
                else:
                    sleep_time = 0
                    transc = 0
            else:
                sleep_time = 5 if 5 >= transc else transc
            sleep(sleep_time)
            transc -= sleep_time
        return True

    def change_window(self, position='last', window=None):
        if window:
            return self.browser.driver.switch_to_window(window)
        else:
            if position == 'last':
                index = -1
            elif position == 'first':
                index = 0
            else:
                index = position
            return self.browser.driver.switch_to_window(self.browser.windows[index].name)

    def open_window(self):
        try:
            self.try_or_wait(lambda: self.browser.driver.execute_script('window.open()'), max_sec=10, raise_classes=[NoSuchWindowException])
        except NoSuchWindowException:
            self.logger.warning('NoSuchWindowException handled in open_window')
            self.change_window()
            self.try_or_wait(lambda: self.browser.driver.execute_script('window.open()'), max_sec=10, raise_classes=[NoSuchWindowException])
        except TryOrWaitTimeoutException:
            self.change_window()
        self.sleep(0.6)

    def visit(self,url):
        return self.try_or_wait(lambda:self.browser.visit('https://{}'.format(url)))



    def get_email(self):
        self.change_window(position=0)
        self.visit('owlymail.com')
        self.try_or_wait(lambda:self.browser.find_by_css('.tm-create > form:nth-child(3) > input:nth-child(2)').click())
        email= self.try_or_wait(lambda:self.browser.find_by_id('current-id').value)
        # TelegramBot.send_notification(mensage=email,types='email creado con exito \n Email: ')
        return email

    def confirm_rev_email(self):
        self.change_window(position=0)
        self.browser.reload()
        self.sleep(time=60)
        self.browser.reload()
        self.sleep(time=30)
        self.browser.reload()
        # self.try_or_wait(lambda:self.browser.find_by_text('Verify your Rev.ai Account').click())
        # self.try_or_wait(lambda:self.browser.find_by_css('#mails  > div:nth-child(2) > div:nth-child(2)').click())
        # obtengo la url
        url=self.try_or_wait(lambda:self.browser.find_by_css('tbody:nth-child(1)>tr:nth-child(3)>td:nth-child(1)')[0].html.split('https://')[1].split('">')[0])
        return url


    def RevCreate(self,email='',context={}):
        self.change_window(position=1)
        self.visit("www.rev.ai/auth/signup")
        self.try_or_wait(lambda:self.browser.find_by_id('signup-firstname').fill(context['first_name']))
        self.try_or_wait(lambda:self.browser.find_by_id('signup-lastname').fill(context['last_name']))
        self.try_or_wait(lambda:self.browser.find_by_id('signup-email').fill(email))
        self.try_or_wait(lambda:self.browser.find_by_id('signup-password').fill(context['password']))
        self.try_or_wait(lambda:self.browser.find_by_id('signup-confirm-password').fill(context['password']))
        self.try_or_wait(lambda:self.browser.find_by_id('signup-submit-button').click())
        return True
    
    def loginRev(self,url_confirm='',email='',password=''):
        # login
        self.change_window(position=1)
        self.visit(url_confirm)
        self.try_or_wait(lambda:self.browser.find_by_id('login-email').fill(email))
        self.try_or_wait(lambda:self.browser.find_by_id('login-password').fill(password))
        self.try_or_wait(lambda:self.browser.find_by_id('login-submit-button').click())
        # encontrar token
        self.try_or_wait(lambda:self.browser.find_by_id('dashboard-sidebar-access-token').click())
        self.try_or_wait(lambda:self.browser.find_by_id('access-token-generate-btn').click())
        # self.sleep(time=15)
        self.try_or_wait(lambda:self.browser.find_by_id('account-generate-token-btn').click())
        self.sleep(time=6)
        token=self.try_or_wait(lambda:self.browser.find_by_css('.access-token').value)
        TelegramBot.send_notification(mensage=token,types='Token Generado con exito \n Token')
        return True

class PersonRandom(object):
    def person():
        first_name=random.choice(list(data_person.first_name()))['first_name']
        last_name=random.choice(list(data_person.last_name()))['last_name']
        passw1=random.choice(list(data_person.password()))
        passw2=random.choice(list(data_person.password()))
        numero_random1=int(random.random()*1000)
        numero_random2=int(random.random()*1000)
        password='{}{}{}{}{}{}'.format(numero_random1,first_name,last_name,passw1,passw2,numero_random2)
        person= {'first_name':first_name,'last_name':last_name,'password':password}
        return person


class TelegramBot(object):
    """docstring for TelegramBot"""
    update_id = None

    def send_notification(mensage='',types=''):
        update_id=None
        # global update_id
        # obtener el caht_id
        bot = telegram.Bot(settings.TELEGRAM_TOKEN_BOT)
        text="{a}:{b}".format(a=types,b=mensage)
        # print('hol')
        try:
            update_id = bot.get_updates()[0].update_id
        except IndexError:
            update_id = None
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        try:
            # chat_id=bot.get_updates(offset=update_id, timeout=10)[0]['message']['chat']['id']
            chat_id=settings.TELEGRAM_CHAT_ID
            text="{a}:  {b}".format(a=types,b=mensage)
            notification=bot.send_message(chat_id,text)
            pass
        except telegram.error.TimedOut:
            raise

        return True


main()




