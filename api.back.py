import logging

from aiohttp import web

from functions import update_file_data, get_file_data

hdlr = logging.FileHandler('api.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger = logging.getLogger('api_logger')
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)


def set_action(request):
    post_data = yield from request.json()
    print("received: ", repr(post_data))
    action_data = {}
    action_data['action'] = post_data['action']
    action_data['data'] = post_data['data']
    data = get_file_data()
    actions_to_perform = data['actions_to_perform']
    actions_to_perform.append(action_data)
    update_file_data(actions_to_perform=actions_to_perform)
    data = {'status': True}
    return web.json_response(data)  



app = web.Application()
app.router.add_post('/set_action', set_action)

if __name__ == '__main__':
    web.run_app(app, port=8084)